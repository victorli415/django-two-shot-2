from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)

# Create your views here.
@login_required(login_url="")
def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {
        "list_of_receipt_objects": receipt.filter(purchaser=request.user),
    }
    return render(request, "receipts/list.html", context)


@login_required(login_url="")
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt = form.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "create_receipt_form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required(login_url="")
def category_list(request):
    expenses = ExpenseCategory.objects.all()
    context = {
        "list_of_category_objects": expenses.filter(owner=request.user),
    }
    return render(request, "receipts/category_list.html", context)


@login_required(login_url="")
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            catergory = form.save(False)
            catergory.owner = request.user
            catergory = form.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {
        "create_category_form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required(login_url="")
def account_list(request):
    accounts = Account.objects.all()
    context = {
        "list_of_account_objects": accounts.filter(owner=request.user),
    }
    return render(request, "receipts/account_list.html", context)


@login_required(login_url="")
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account = form.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "create_account_form": form,
    }
    return render(request, "receipts/create_account.html", context)
